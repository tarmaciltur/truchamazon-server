package db

import (
	"log"
	"sync"

	"github.com/elastic/go-elasticsearch/v7"
)

type ElasticSearch struct {
	client *elasticsearch.Client
}

var (
	es   *ElasticSearch
	once sync.Once
)

func New() *ElasticSearch {
	once.Do(initDB)

	return es
}

func initDB() {
	c, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Panic(err.Error())
	}

	es = &ElasticSearch{client: c}
}
