package db

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/elastic/go-elasticsearch/v7/esapi"
	"gitlab.com/tarmaciltur/truchamazon-server/internal/models"
)

type ProductRepository struct {
	db *ElasticSearch
}

type searchResponse struct {
	Hits searchHits `json:"hits"`
}

type searchHits struct {
	Hits []elasticProductResponse `json:"hits"`
}

type elasticProductResponse struct {
	Id      string         `json:"_id"`
	Product models.Product `json:"_source"`
}

func NewProductRepository(es *ElasticSearch) *ProductRepository {
	return &ProductRepository{db: es}
}

func (pr ProductRepository) GetAll() ([]models.Product, error) {
	response, err := pr.db.client.Search(func(request *esapi.SearchRequest) {
		request.Index = []string{"products"}
		body := `
			{
				"query": {
					"match_all": {}
				}
			}
		`
		request.Body = strings.NewReader(body)
	})
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	var res searchResponse
	err = json.Unmarshal(data, &res)
	if err != nil {
		return nil, err
	}

	var products []models.Product
	for _, hit := range res.Hits.Hits {
		hit.Product.Id = hit.Id
		products = append(products, hit.Product)
	}

	return products, nil
}

func (pr ProductRepository) GetOne(id string) (models.Product, error) {
	response, err := pr.db.client.Search(func(request *esapi.SearchRequest) {
		request.Index = []string{"products"}
		body := `
			{
				"query": {
					"terms": {
						"_id": %v
					}
				}
			}
		`
		body = fmt.Sprintf(body, id)
		request.Body = strings.NewReader(body)
	})
	if err != nil {
		return models.Product{}, err
	}
	defer response.Body.Close()

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return models.Product{}, err
	}

	var res searchResponse
	err = json.Unmarshal(data, &res)
	if err != nil {
		return models.Product{}, err
	}

	product := res.Hits.Hits[0].Product
	product.Id = res.Hits.Hits[0].Id

	return product, nil
}
