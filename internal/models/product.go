package models

type ProductStyle struct {
	Name  string `json:"name,omitempty"`
	Image string `json:"image,omitempty"`
}

type Product struct {
	Id         string         `json:"id,omitempty"`
	Title      string         `json:"title,omitempty"`
	Descrition string         `json:"description,omitempty"`
	Styles     []ProductStyle `json:"styles,omitempty"`
	Features   []string       `json:"features,omitempty"`
}
