package repositories

import "gitlab.com/tarmaciltur/truchamazon-server/internal/models"

type Product interface {
	GetAll() ([]models.Product, error)
	GetOne(id string) (models.Product, error)
}
