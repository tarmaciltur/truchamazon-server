package v1

import (
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/tarmaciltur/truchamazon-server/internal/db"
	"gitlab.com/tarmaciltur/truchamazon-server/internal/server/response"
)

const ApiPath string = "/api/v1"

func New() http.Handler {
	r := chi.NewRouter()

	pr := &ProductRouter{Repository: db.NewProductRepository(db.New())}

	r.Mount(ProductPath, pr.Routes())

	return r
}

func getId(w http.ResponseWriter, r *http.Request) (string, bool) {
	id := chi.URLParam(r, "id")
	if id == "" {
		response.HTTPError(w, r, http.StatusBadRequest, "no id in request")
		return "", false
	}

	return id, true
}
