package v1

import (
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/tarmaciltur/truchamazon-server/internal/repositories"
	"gitlab.com/tarmaciltur/truchamazon-server/internal/server/response"
)

const ProductPath string = "/products"

type ProductRouter struct {
	Repository repositories.Product
}

func (pr ProductRouter) Routes() http.Handler {
	r := chi.NewRouter()

	r.Get("/", pr.GetAllHandler)
	r.Get("/{id}", pr.GetOneHandler)

	return r
}

func (pr ProductRouter) GetAllHandler(w http.ResponseWriter, r *http.Request) {
	products, err := pr.Repository.GetAll()
	if err != nil {
		response.HTTPError(w, r, http.StatusBadRequest, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, response.Map{"products": products})
}

func (pr ProductRouter) GetOneHandler(w http.ResponseWriter, r *http.Request) {
	id, ok := getId(w, r)
	if !ok {
		return
	}

	product, err := pr.Repository.GetOne(id)
	if err != nil {
		response.HTTPError(w, r, http.StatusOK, err.Error())
	}

	response.JSON(w, r, http.StatusOK, response.Map{"user": product})
}
