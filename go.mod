module gitlab.com/tarmaciltur/truchamazon-server

go 1.16

require (
	github.com/elastic/go-elasticsearch/v7 v7.14.0
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/cors v1.2.0
	github.com/joho/godotenv v1.3.0
	gitlab.com/softwareperonista/agenda-docente-backend v0.0.0-20210826184107-80987b299e59
)
