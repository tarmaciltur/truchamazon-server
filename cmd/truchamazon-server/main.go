package main

import (
	"log"
	"os"
	"os/signal"

	_ "github.com/joho/godotenv/autoload"
	"gitlab.com/softwareperonista/agenda-docente-backend/pkg/checkenv"
	"gitlab.com/tarmaciltur/truchamazon-server/internal/server"
)

func main() {
	port, err := checkenv.Checkempty("PORT")
	if err != nil {
		log.Fatal(err)
	}

	serv, err := server.New(port)
	if err != nil {
		log.Fatal(err)
	}

	go serv.Start()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
}
